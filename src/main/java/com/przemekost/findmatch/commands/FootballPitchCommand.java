package com.przemekost.findmatch.commands;

import com.przemekost.findmatch.domain.InOrOut;
import com.przemekost.findmatch.domain.Surface;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class FootballPitchCommand {

    private long id;
    private String footballPitchName;
    private int minAmountOfPlayers;
    private int maxAmountOfPlayers;
    private InOrOut inOrOut;
    private Surface surface;
    private LocationCommand location;
}
