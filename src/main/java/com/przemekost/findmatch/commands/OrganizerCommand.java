package com.przemekost.findmatch.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class OrganizerCommand {

    private Long id;
    private String organizerName;
    private String organizerLastName;
    private String organizerEmailAddress;
    private String organizerTelephoneNumber;
}
