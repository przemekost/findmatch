package com.przemekost.findmatch.commands;

import com.przemekost.findmatch.domain.Event;
import com.przemekost.findmatch.domain.FavouritePosition;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.joda.time.LocalDate;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class PlayerCommand {

    private Long id;
    private String playerName;
    private String playerLastName;
    private String playerEmailAddress;
    private String playerTelephoneNumber;
    private FavouritePosition favouritePosition;
    private Set<Event> events = new HashSet<>();
    private String birthdayDate;

}
