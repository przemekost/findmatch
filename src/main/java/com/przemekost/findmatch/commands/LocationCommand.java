package com.przemekost.findmatch.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Setter
@Getter
public class LocationCommand {

    private long id;
    private String cityName;
    private String streetName;
    private int streetNumber;
    private Byte[] map;
    private Set<FootballPitchCommand> footballPitches = new HashSet<>();

}
