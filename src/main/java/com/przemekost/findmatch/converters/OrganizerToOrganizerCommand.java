package com.przemekost.findmatch.converters;

import com.przemekost.findmatch.commands.OrganizerCommand;
import com.przemekost.findmatch.domain.Organizer;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class OrganizerToOrganizerCommand implements Converter<Organizer, OrganizerCommand> {

    @Synchronized
    @Nullable
    @Override
    public OrganizerCommand convert(Organizer source) {

        if (source == null) {
            return null;
        }

        final OrganizerCommand organizerCommand = new OrganizerCommand();
        organizerCommand.setId(source.getId());
        organizerCommand.setOrganizerName(source.getOrganizerName());
        organizerCommand.setOrganizerLastName(source.getOrganizerLastName());
        organizerCommand.setOrganizerEmailAddress(source.getOrganizerEmailAddress());
        organizerCommand.setOrganizerTelephoneNumber(source.getOrganizerTelephoneNumber());

        return organizerCommand;
    }
}
