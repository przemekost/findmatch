package com.przemekost.findmatch.converters;

import com.przemekost.findmatch.commands.LocationCommand;
import com.przemekost.findmatch.domain.Location;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class LocationToLocationCommand implements Converter<Location, LocationCommand> {

    FootballPitchToFootballPitchCommand footballPitchCommandConverter;

    public LocationToLocationCommand(FootballPitchToFootballPitchCommand footballPitchCommandConverter) {
        this.footballPitchCommandConverter = footballPitchCommandConverter;
    }

    @Synchronized
    @Nullable
    @Override
    public LocationCommand convert(Location source) {

        if (source == null) {
            return null;
        }

        final LocationCommand locationCommand = new LocationCommand();
        locationCommand.setId(source.getId());
        locationCommand.setCityName(source.getCityName());
        locationCommand.setStreetName(source.getStreetName());
        locationCommand.setStreetNumber(source.getStreetNumber());
        locationCommand.setMap(source.getMap());

        if (source.getFootballPitches() != null && source.getFootballPitches().size()>0){
            source.getFootballPitches()
                    .forEach(footballPitchCommand -> locationCommand.getFootballPitches().add(footballPitchCommandConverter.convert(footballPitchCommand)));
        }


        return locationCommand;
    }
}
