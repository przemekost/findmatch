package com.przemekost.findmatch.converters;

import com.przemekost.findmatch.commands.EventCommand;
import com.przemekost.findmatch.domain.Event;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class EventToEventCommand implements Converter<Event, EventCommand> {

    private final OrganizerToOrganizerCommand organizerToOrganizerCommand;
    private final LocationToLocationCommand locationToLocationCommand;

    public EventToEventCommand(OrganizerToOrganizerCommand organizerToOrganizerCommand, LocationToLocationCommand locationToLocationCommand) {
        this.organizerToOrganizerCommand = organizerToOrganizerCommand;
        this.locationToLocationCommand = locationToLocationCommand;
    }

    @Synchronized
    @Nullable
    @Override
    public EventCommand convert(Event source) {

        if (source == null) {
            return null;
        }

        final EventCommand eventCommand = new EventCommand();
        eventCommand.setId(source.getId());
        eventCommand.setEventName(source.getEventName());
        eventCommand.setStartTime(source.getStartTime());
        eventCommand.setEndTime(source.getEndTime());
        eventCommand.setPrice(source.getPrice());
        eventCommand.setLocation(locationToLocationCommand.convert(source.getLocation()));

        if (source.getOrganizers() != null && source.getOrganizers().size() > 0) {
            source.getOrganizers()
                    .forEach(organizer -> eventCommand.getOrganizers().add(organizerToOrganizerCommand.convert(organizer)));
        }



        return eventCommand;
    }
}
