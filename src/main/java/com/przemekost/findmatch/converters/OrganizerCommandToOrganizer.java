package com.przemekost.findmatch.converters;

import com.przemekost.findmatch.commands.OrganizerCommand;
import com.przemekost.findmatch.domain.Organizer;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class OrganizerCommandToOrganizer implements Converter<OrganizerCommand, Organizer> {

    @Synchronized
    @Nullable
    @Override
    public Organizer convert(OrganizerCommand source) {

        final Organizer organizer = new Organizer();
        organizer.setId(source.getId());
        organizer.setOrganizerName(source.getOrganizerName());
        organizer.setOrganizerLastName(source.getOrganizerLastName());
        organizer.setOrganizerEmailAddress(source.getOrganizerEmailAddress());
        organizer.setOrganizerTelephoneNumber(source.getOrganizerTelephoneNumber());

        return organizer;
    }
}
