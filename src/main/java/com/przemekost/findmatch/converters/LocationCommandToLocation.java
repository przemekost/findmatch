package com.przemekost.findmatch.converters;

import com.przemekost.findmatch.commands.LocationCommand;
import com.przemekost.findmatch.domain.Location;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class LocationCommandToLocation implements Converter<LocationCommand, Location> {

    FootballPitchCommandToFootballPitch footballPitchConverter;

    public LocationCommandToLocation(FootballPitchCommandToFootballPitch footballPitchConverter) {
        this.footballPitchConverter = footballPitchConverter;
    }

    @Synchronized
    @Nullable
    @Override
    public Location convert(LocationCommand source) {

        if (source == null) {
            return null;
        }

        final Location location = new Location();
        location.setId(source.getId());
        location.setCityName(source.getCityName());
        location.setStreetName(source.getStreetName());
        location.setStreetNumber(source.getStreetNumber());
        location.setMap(source.getMap());

        if (source.getFootballPitches() != null && source.getFootballPitches().size()>0){
            source.getFootballPitches()
                    .forEach(footballPitchCommand -> location.getFootballPitches().add(footballPitchConverter.convert(footballPitchCommand)));
        }


        return location;
    }
}
