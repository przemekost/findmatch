package com.przemekost.findmatch.converters;

import com.przemekost.findmatch.commands.PlayerCommand;
import com.przemekost.findmatch.domain.Player;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;


@Component
public class PlayerToPlayerCommand implements Converter<Player, PlayerCommand> {

    @Synchronized
    @Nullable
    @Override
    public PlayerCommand convert(Player source) {

        final PlayerCommand playerCommand = new PlayerCommand();
        playerCommand.setId(source.getId());
        playerCommand.setPlayerName(source.getPlayerName());
        playerCommand.setPlayerLastName(source.getPlayerLastName());
        playerCommand.setPlayerEmailAddress(source.getPlayerEmailAddress());
        playerCommand.setPlayerTelephoneNumber(source.getPlayerTelephoneNumber());
        playerCommand.setFavouritePosition(source.getFavouritePosition());
        playerCommand.setBirthdayDate(source.getBirthdayDate());

        if (source.getEvents() != null && !source.getEvents().isEmpty()){
            playerCommand.setEvents(source.getEvents());
        }


        return playerCommand;
    }
}
