package com.przemekost.findmatch.converters;

import com.przemekost.findmatch.commands.PlayerCommand;
import com.przemekost.findmatch.domain.Player;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;


@Component
public class PlayerCommandToPlayer implements Converter<PlayerCommand, Player> {

    @Synchronized
    @Nullable
    @Override
    public Player convert(PlayerCommand source) {

        final Player player = new Player();
        player.setId(source.getId());
        player.setPlayerName(source.getPlayerName());
        player.setPlayerLastName(source.getPlayerLastName());
        player.setPlayerEmailAddress(source.getPlayerEmailAddress());
        player.setPlayerTelephoneNumber(source.getPlayerTelephoneNumber());
        player.setFavouritePosition(source.getFavouritePosition());
        player.setBirthdayDate(source.getBirthdayDate());

        if (source.getEvents() != null && !source.getEvents().isEmpty()) {
            player.setEvents(source.getEvents());
        }

        return player;
    }
}
