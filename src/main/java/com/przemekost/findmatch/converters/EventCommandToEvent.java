package com.przemekost.findmatch.converters;

import com.przemekost.findmatch.commands.EventCommand;
import com.przemekost.findmatch.domain.Event;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class EventCommandToEvent implements Converter<EventCommand, Event> {

    OrganizerCommandToOrganizer organizerConverter;

    LocationCommandToLocation locationConverter;



    @Synchronized
    @Nullable
    @Override
    public Event convert(EventCommand source) {

        if (source == null) {
            return null;
        }

        final Event event = new Event();
        event.setId(source.getId());
        event.setEventName(source.getEventName());
        event.setStartTime(source.getStartTime());
        event.setEndTime(source.getEndTime());
        event.setPrice(source.getPrice());
        event.setLocation(locationConverter.convert(source.getLocation()));

        if (source.getOrganizers() != null && source.getOrganizers().size()>0){
            source.getOrganizers()
                    .forEach(organizer -> event.getOrganizers().add(organizerConverter.convert(organizer)));
        }

//        if (source.getPlayers() != null && source.getPlayers().size()>0){
//            source.getPlayers()
//                    .forEach(playerCommand -> event.getPlayers().add(playerCommand));
//        }

        return event;
    }
}
