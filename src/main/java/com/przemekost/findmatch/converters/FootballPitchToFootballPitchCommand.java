package com.przemekost.findmatch.converters;


import com.przemekost.findmatch.commands.FootballPitchCommand;
import com.przemekost.findmatch.domain.FootballPitch;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class FootballPitchToFootballPitchCommand implements Converter<FootballPitch, FootballPitchCommand> {

    @Synchronized
    @Nullable
    @Override
    public FootballPitchCommand convert(FootballPitch source) {

        if (source == null) {
            return null;
        }

        final FootballPitchCommand footballPitchCommand = new FootballPitchCommand();
        footballPitchCommand.setId(source.getId());
        footballPitchCommand.setFootballPitchName(source.getFootballPitchName());
        footballPitchCommand.setMinAmountOfPlayers(source.getMinAmountOfPlayers());
        footballPitchCommand.setMaxAmountOfPlayers(source.getMaxAmountOfPlayers());
        footballPitchCommand.setInOrOut(source.getInOrOut());
        footballPitchCommand.setSurface(source.getSurface());

        return footballPitchCommand;
    }
}
