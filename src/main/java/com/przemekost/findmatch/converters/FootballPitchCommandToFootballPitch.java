package com.przemekost.findmatch.converters;


import com.przemekost.findmatch.commands.FootballPitchCommand;
import com.przemekost.findmatch.domain.FootballPitch;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class FootballPitchCommandToFootballPitch implements Converter<FootballPitchCommand, FootballPitch> {

    @Synchronized
    @Nullable
    @Override
    public FootballPitch convert(FootballPitchCommand source) {

        if (source == null) {
            return null;
        }

        final FootballPitch footballPitch = new FootballPitch();
        footballPitch.setId(source.getId());
        footballPitch.setFootballPitchName(source.getFootballPitchName());
        footballPitch.setMinAmountOfPlayers(source.getMinAmountOfPlayers());
        footballPitch.setMaxAmountOfPlayers(source.getMaxAmountOfPlayers());
        footballPitch.setInOrOut(source.getInOrOut());
        footballPitch.setSurface(source.getSurface());
//        footballPitch.setLocation(source.getLocation());

        return footballPitch;
    }
}
