package com.przemekost.findmatch.bootstrap;


import com.przemekost.findmatch.domain.*;
import com.przemekost.findmatch.repositories.*;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.*;

@Component
public class EventBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private final EventRepository eventRepository;
    private final OrganizerRepository organizerRepository;
    private final PlayerRepository playerRepository;

    public EventBootstrap(EventRepository eventRepository, OrganizerRepository organizerRepository, PlayerRepository playerRepository) {
        this.eventRepository = eventRepository;
        this.organizerRepository = organizerRepository;
        this.playerRepository = playerRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        eventRepository.saveAll(getEvent());
    }

    private List<Event> getEvent() {
        List<Event> events = new ArrayList<>();


        //Organizers
        Optional<Organizer> ostrouchKolobrzeskaOrganizerOptional = organizerRepository.findByOrOrganizerLastName("Ostrouch");
        if (!ostrouchKolobrzeskaOrganizerOptional.isPresent()) {
            throw new RuntimeException("Organizer not found !");
        }

        Organizer ostrouchKolobrzeskaOrganizer = ostrouchKolobrzeskaOrganizerOptional.get();

        Optional<Organizer> kaimWestFootballPitchOrganizerOptional = organizerRepository.findByOrOrganizerLastName("Kaim");
        if (!kaimWestFootballPitchOrganizerOptional.isPresent()) {
            throw new RuntimeException("Organizer not found!");
        }

        Organizer kaimWestFootballPitchOrganizer = kaimWestFootballPitchOrganizerOptional.get();


        //Set 1 organizers
        Set<Organizer> organizersSetOne = new HashSet<>();
        organizersSetOne.add(kaimWestFootballPitchOrganizer);
        organizersSetOne.add(ostrouchKolobrzeskaOrganizer);


        //Set 2 organizers
        Set<Organizer> organizerSetTwo = new HashSet<>();
        organizerSetTwo.add(ostrouchKolobrzeskaOrganizer);




        //Location
        Location gdanskLocation = new Location();
        gdanskLocation.setCityName("Gdansk");
        gdanskLocation.setStreetName("Kołobrzeska Street");
        gdanskLocation.setStreetNumber(11);

        Location dabrowaLocation = new Location();
        dabrowaLocation.setCityName("Dąbrowa Górnicza");
        dabrowaLocation.setStreetName("Zachodnia Street");
        dabrowaLocation.setStreetNumber(11);


        FootballPitch kolobrzeskaFootballPitch = new FootballPitch();
        kolobrzeskaFootballPitch.setFootballPitchName("Kołobrzeska Football Pitch");
        kolobrzeskaFootballPitch.setSurface(Surface.ARTIFICIAL_TURF);
        kolobrzeskaFootballPitch.setInOrOut(InOrOut.OUTDOOR);
        kolobrzeskaFootballPitch.setMaxAmountOfPlayers(14);
        kolobrzeskaFootballPitch.setMinAmountOfPlayers(10);
        kolobrzeskaFootballPitch.setLocation(gdanskLocation);


        FootballPitch westFootballPitch = new FootballPitch();
        westFootballPitch.setFootballPitchName("West Football Pitch");
        westFootballPitch.setMinAmountOfPlayers(10);
        westFootballPitch.setMaxAmountOfPlayers(12);
        westFootballPitch.setInOrOut(InOrOut.OUTDOOR);
        westFootballPitch.setSurface(Surface.ARTIFICIAL_TURF);
        westFootballPitch.setLocation(dabrowaLocation);

        dabrowaLocation.getFootballPitches().add(westFootballPitch);
        gdanskLocation.getFootballPitches().add(kolobrzeskaFootballPitch);

        //getPlayer
        Optional<Player> przemekPlayerOptional = playerRepository.findByPlayerName("Przemyslaw");
        if (!przemekPlayerOptional.isPresent()) {
            throw new RuntimeException("Expected player not found!");
        }

        Optional<Player> jakubPlayerOptional = playerRepository.findByPlayerName("Jakub");
        if (!jakubPlayerOptional.isPresent()) {
            throw new RuntimeException("Expected player not found!");
        }


        Player przemekPlayer = przemekPlayerOptional.get();
        przemekPlayer.setFavouritePosition(FavouritePosition.MIDFIELDER);

        Player jakubPlayer = jakubPlayerOptional.get();
        jakubPlayer.setFavouritePosition(FavouritePosition.ATTACKER);


        Set<Player> playersSetOne = new HashSet<>();
        playersSetOne.add(przemekPlayer);
        playersSetOne.add(jakubPlayer);

        Set<Player> playersSetTwo = new HashSet<>();
        playersSetTwo.add(przemekPlayer);

        Event kolobrzeskaEvent = new Event();
        kolobrzeskaEvent.setOrganizers(organizersSetOne);
        kolobrzeskaEvent.setEventName("Kołobrzeska Event");
        kolobrzeskaEvent.setStartTime((LocalDateTime.of(2017, Month.NOVEMBER, 15, 19, 00, 00)));
        kolobrzeskaEvent.setEndTime((LocalDateTime.of(2017, Month.NOVEMBER, 15, 20, 30, 00)));
        kolobrzeskaEvent.setPlayers(playersSetOne);
        kolobrzeskaEvent.setLocation(gdanskLocation);
        kolobrzeskaEvent.setPrice(12.00);

        Event westEvent = new Event();
        westEvent.setOrganizers(organizerSetTwo);
        westEvent.setEventName("West Event");
        westEvent.setStartTime((LocalDateTime.of(2017, Month.APRIL, 22, 18, 00, 00)));
        westEvent.setEndTime((LocalDateTime.of(2017, Month.APRIL, 22, 19, 30, 00)));
        westEvent.setPlayers(playersSetTwo);
        westEvent.setLocation(dabrowaLocation);
        westEvent.setPrice(10.00);

        events.add(kolobrzeskaEvent);
        events.add(westEvent);


        return events;
    }
}
