package com.przemekost.findmatch.repositories;

import com.przemekost.findmatch.domain.FootballPitch;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface FootballPitchRepository extends CrudRepository<FootballPitch, Long> {

    Optional<FootballPitch> findByFootballPitchName (String footballPitchName);

}
