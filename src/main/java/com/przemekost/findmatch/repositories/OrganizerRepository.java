package com.przemekost.findmatch.repositories;

import com.przemekost.findmatch.domain.Organizer;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface OrganizerRepository extends CrudRepository<Organizer, Long> {

    Optional<Organizer> findByOrOrganizerLastName(String organizerLastName);

}
