package com.przemekost.findmatch.repositories;

import com.przemekost.findmatch.domain.Event;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
public interface EventRepository extends CrudRepository<Event, Long> {

    Optional<Event> findByEventName(String eventName);


}
