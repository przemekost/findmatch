package com.przemekost.findmatch.repositories;

import com.przemekost.findmatch.domain.Player;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface PlayerRepository extends CrudRepository<Player, Long> {

    Optional<Player> findByPlayerName(String playerName);

}
