package com.przemekost.findmatch.repositories;

import com.przemekost.findmatch.domain.Location;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface LocationRepository extends CrudRepository<Location, Long> {

    Optional<Location> findByCityName(String cityName);

}
