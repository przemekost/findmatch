package com.przemekost.findmatch.services;

import com.przemekost.findmatch.commands.OrganizerCommand;
import com.przemekost.findmatch.converters.OrganizerCommandToOrganizer;
import com.przemekost.findmatch.converters.OrganizerToOrganizerCommand;
import com.przemekost.findmatch.domain.Event;
import com.przemekost.findmatch.domain.Organizer;
import com.przemekost.findmatch.domain.Player;
import com.przemekost.findmatch.repositories.EventRepository;
import com.przemekost.findmatch.repositories.OrganizerRepository;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.weaver.ast.Or;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Slf4j
@Service
public class OrganizerServiceImpl implements OrganizerService {

    private final OrganizerRepository organizerRepository;

    private OrganizerCommandToOrganizer organizerCommandToOrganizer;

    private OrganizerToOrganizerCommand organizerToOrganizerCommand;

    private EventRepository eventRepository;

    public OrganizerServiceImpl(OrganizerRepository organizerRepository,
                                OrganizerCommandToOrganizer organizerCommandToOrganizer,
                                OrganizerToOrganizerCommand organizerToOrganizerCommand,
                                EventRepository eventRepository) {
        this.organizerRepository = organizerRepository;
        this.organizerCommandToOrganizer = organizerCommandToOrganizer;
        this.organizerToOrganizerCommand = organizerToOrganizerCommand;
        this.eventRepository = eventRepository;
    }

    @Override
    public Organizer getOrganizerById(Long id) {

        Optional<Organizer> organizerOptional = organizerRepository.findById(id);
        if (!organizerOptional.isPresent()) {
            throw new RuntimeException("Expected organizer not found");
        }

        return organizerOptional.get();
    }

    @Override
    @Transactional
    public OrganizerCommand saveOrganizerCommand(OrganizerCommand organizerCommand) {

        Organizer detachedOrganizer = organizerCommandToOrganizer.convert(organizerCommand);
        Organizer savedOrganizer = organizerRepository.save(detachedOrganizer);
        log.debug("Saved Organizer ID: " + savedOrganizer.getId());

        return organizerToOrganizerCommand.convert(savedOrganizer);
    }

    @Override
    public OrganizerCommand getOrganizerCommandByEventIdAndOrganizerId(Long eventId, Long organizerId) {

        Optional<Event> eventOptional = eventRepository.findById(eventId);

        if (!eventOptional.isPresent()) {
            log.error("Expected EventOptional not exist, event Id: " + eventId);
        }

        Event event = eventOptional.get();

        Optional<OrganizerCommand> organizerCommandOptional = event.getOrganizers().stream()
                .filter(organizer -> organizer.getId().equals(organizerId))
                .map(organizer -> organizerToOrganizerCommand.convert(organizer))
                .findFirst();

        if (!organizerCommandOptional.isPresent()) {
            log.error("Organizer not found, id: " + organizerId);
        }
        return organizerCommandOptional.get();
    }

    @Override
    public void deleteOrganizerFromEventByEventIdAndOrganizerId(Long eventId, Long organizerId) {

        Optional<Event> eventOptional = eventRepository.findById(eventId);
        Event event = null;

        if (eventOptional.isPresent()) {
            event = eventOptional.get();
            Optional<Organizer> organizerOptional = event.getOrganizers()
                    .stream()
                    .filter(organizer -> organizer.getId().equals(organizerId))
                    .findFirst();


            if (organizerOptional.isPresent()) {
                Organizer organizer = organizerOptional.get();
                organizer.setEvents(null);
                event.getOrganizers().remove(organizer);
                eventRepository.save(event);
            }
        } else {
            log.error("There are no event with that ID: " + eventId);
        }

    }

    @Override
    public void deleteOrganizerFromDB(Long organizerId) {

        Optional<Organizer> organizerOptional = organizerRepository.findById(organizerId);
        if (organizerOptional.isPresent()){
            Organizer organizer = organizerOptional.get();
            organizerRepository.delete(organizer);
        } else {
            log.error("There are no organizer with this ID");
        }

    }

}
