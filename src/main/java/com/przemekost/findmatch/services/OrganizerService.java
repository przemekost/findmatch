package com.przemekost.findmatch.services;

import com.przemekost.findmatch.commands.OrganizerCommand;
import com.przemekost.findmatch.domain.Event;
import com.przemekost.findmatch.domain.Organizer;

import java.util.Set;

public interface OrganizerService {

    Organizer getOrganizerById(Long id);

    OrganizerCommand saveOrganizerCommand(OrganizerCommand organizerCommand);

    OrganizerCommand getOrganizerCommandByEventIdAndOrganizerId(Long eventId, Long organizerId);

    void deleteOrganizerFromEventByEventIdAndOrganizerId(Long eventId, Long organizerId);

    void deleteOrganizerFromDB(Long organizerId);
}
