package com.przemekost.findmatch.services;

import com.przemekost.findmatch.commands.PlayerCommand;
import com.przemekost.findmatch.domain.Player;

import java.util.HashSet;
import java.util.Set;

public interface PlayerService {

    Player getPlayerById(Long id);

    PlayerCommand savePlayerCommand(PlayerCommand playerCommand);

    PlayerCommand getPlayerByEventIdAndPlayerId(Long eventId, Long playerId);

    Set<PlayerCommand> listAllPlayers();

    PlayerCommand getPlayerCommandById(Long id);

    void deletePlayerFromEventByEventIdAndPlayerId(Long eventId, Long playerId);

    void deletePlayerFromDbByPlayerId(Long playerId);

    Set<Long> eventIdsFromSpecifiedPlayer(Player player);
}
