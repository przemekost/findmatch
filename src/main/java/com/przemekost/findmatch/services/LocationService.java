package com.przemekost.findmatch.services;

import com.przemekost.findmatch.commands.LocationCommand;
import com.przemekost.findmatch.domain.Location;

public interface LocationService {

    Location getLocationById(Long id);

    LocationCommand saveLocationCommand(LocationCommand locationCommand);

}
