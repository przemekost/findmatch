package com.przemekost.findmatch.services;

import com.przemekost.findmatch.commands.LocationCommand;
import com.przemekost.findmatch.converters.LocationCommandToLocation;
import com.przemekost.findmatch.converters.LocationToLocationCommand;
import com.przemekost.findmatch.domain.Location;
import com.przemekost.findmatch.repositories.LocationRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Slf4j
@Service
public class LocationServiceImpl implements LocationService {

    LocationRepository locationRepository;
    LocationCommandToLocation locationCommandToLocation;
    LocationToLocationCommand locationToLocationCommand;

    public LocationServiceImpl(LocationRepository locationRepository,
                               LocationCommandToLocation locationCommandToLocation,
                               LocationToLocationCommand locationToLocationCommand) {
        this.locationRepository = locationRepository;
        this.locationCommandToLocation = locationCommandToLocation;
        this.locationToLocationCommand = locationToLocationCommand;
    }

    @Override
    public Location getLocationById(Long id) {

        Optional<Location> locationOptional = locationRepository.findById(id);
        if (!locationOptional.isPresent()) {
            throw new RuntimeException("Expected location not found");
        }

        return locationOptional.get();
    }

    @Override
    @Transactional
    public LocationCommand saveLocationCommand(LocationCommand locationCommand) {

        Location detachedLocation = locationCommandToLocation.convert(locationCommand);

        Location savedLocation = locationRepository.save(detachedLocation);
        log.debug("Saved LocationID: " + savedLocation.getId());

        return locationToLocationCommand.convert(savedLocation);
    }
}
