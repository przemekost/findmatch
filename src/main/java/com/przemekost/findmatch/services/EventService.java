package com.przemekost.findmatch.services;

import com.przemekost.findmatch.commands.EventCommand;
import com.przemekost.findmatch.domain.Event;

import java.util.Set;

public interface EventService {

    Set<Event> getAllEvents();

    Event getEventById(Long id);

    EventCommand saveEventCommand(EventCommand eventCommand);

    EventCommand findEventCommandById(Long id);

    void deleteById(Long idToDelete);

}
