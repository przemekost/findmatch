package com.przemekost.findmatch.services;

import com.przemekost.findmatch.commands.EventCommand;
import com.przemekost.findmatch.converters.EventCommandToEvent;
import com.przemekost.findmatch.converters.EventToEventCommand;
import com.przemekost.findmatch.domain.Event;
import com.przemekost.findmatch.repositories.EventRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Slf4j
@Service
public class EventServiceImpl implements EventService{

    private final EventRepository eventRepository;
    private final EventCommandToEvent eventCommandToEvent;
    private final EventToEventCommand eventToEventCommand;

    public EventServiceImpl(EventRepository eventRepository,
                            EventCommandToEvent eventCommandToEvent,
                            EventToEventCommand eventToEventCommand) {
        this.eventRepository = eventRepository;
        this.eventCommandToEvent = eventCommandToEvent;
        this.eventToEventCommand = eventToEventCommand;
    }

    @Override
    public Set<Event> getAllEvents() {
        Set<Event> events = new HashSet<>();

        eventRepository.findAll().iterator().forEachRemaining(events::add);
        return events;
    }

    @Override
    public Event getEventById(Long id) {

        Optional<Event> eventByIdOptional = eventRepository.findById(id);
        if (!eventByIdOptional.isPresent()){
            throw new RuntimeException("Expected event not Found");
        }

        return eventByIdOptional.get();
    }

    @Override
    @Transactional
    public EventCommand saveEventCommand(EventCommand eventCommand) {

        Event detachedEvent = eventCommandToEvent.convert(eventCommand);

        Event savedEvent = eventRepository.save(detachedEvent);
        log.debug("Saved EventId: "+savedEvent.getId());

        return eventToEventCommand.convert(savedEvent);
    }

    @Override
    @Transactional
    public EventCommand findEventCommandById(Long l) {
        return eventToEventCommand.convert(getEventById(l));
    }


    @Override
    public void deleteById(Long idToDelete) {
        eventRepository.deleteById(idToDelete);
    }
}
