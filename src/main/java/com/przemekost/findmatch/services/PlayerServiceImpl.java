package com.przemekost.findmatch.services;

import com.przemekost.findmatch.commands.PlayerCommand;
import com.przemekost.findmatch.converters.PlayerCommandToPlayer;
import com.przemekost.findmatch.converters.PlayerToPlayerCommand;
import com.przemekost.findmatch.domain.Event;
import com.przemekost.findmatch.domain.Player;
import com.przemekost.findmatch.repositories.EventRepository;
import com.przemekost.findmatch.repositories.PlayerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@Service
public class PlayerServiceImpl implements PlayerService {

    private final PlayerRepository playerRepository;
    private PlayerCommandToPlayer playerCommandToPlayer;
    private PlayerToPlayerCommand playerToPlayerCommand;
    private EventRepository eventRepository;
    private Player player;

    public PlayerServiceImpl(PlayerRepository playerRepository,
                             PlayerCommandToPlayer playerCommandToPlayer,
                             PlayerToPlayerCommand playerToPlayerCommand,
                             EventRepository eventRepository) {
        this.playerRepository = playerRepository;
        this.playerCommandToPlayer = playerCommandToPlayer;
        this.playerToPlayerCommand = playerToPlayerCommand;
        this.eventRepository = eventRepository;
    }

    public Player getPlayerById(Long id) {

        Optional<Player> playerOptional = playerRepository.findById(id);

        if (!playerOptional.isPresent()) {
            throw new RuntimeException("Expected player not found");
        }
        return playerOptional.get();
    }

    @Override
    @Transactional
    public PlayerCommand savePlayerCommand(PlayerCommand playerCommand) {

        Player detachedPlayer = playerCommandToPlayer.convert(playerCommand);

        Player savedPlayer = playerRepository.save(detachedPlayer);
        log.debug("Saved Player ID: " + savedPlayer.getId());

        return playerToPlayerCommand.convert(savedPlayer);
    }

    @Override
    public PlayerCommand getPlayerByEventIdAndPlayerId(Long eventId, Long playerId) {

        Optional<Event> eventOptional = eventRepository.findById(eventId);

        eventOptional.ifPresent(event -> player = event.getPlayers().stream().filter(player -> player.getId()
                .equals(playerId))
                .findAny().orElse(null));

        return playerToPlayerCommand.convert(player);
    }

    @Override
    public Set<PlayerCommand> listAllPlayers() {
        return StreamSupport.stream(playerRepository.findAll()
                .spliterator(), false)
                .map(playerToPlayerCommand::convert)
                .collect(Collectors.toSet());
    }

    @Override
    public PlayerCommand getPlayerCommandById(Long id) {

        Optional<Player> playerOptional = playerRepository.findById(id);

        if (playerOptional.isPresent()) {
            return playerToPlayerCommand.convert(playerOptional.get());
        }
        return null;
    }

    @Override
    public void deletePlayerFromEventByEventIdAndPlayerId(Long eventId, Long playerId) {

        Optional<Event> eventOptional = eventRepository.findById(eventId);

        if (eventOptional.isPresent()) {
            Event event = eventOptional.get();
            Optional<Player> searchedPlayerOptional = event.getPlayers()
                    .stream()
                    .filter(player1 -> player1.getId().equals(playerId))
                    .findFirst();

            if (searchedPlayerOptional.isPresent()) {
                Player player = searchedPlayerOptional.get();
                player.setEvents(null);
                event.getPlayers().remove(player);
                eventRepository.save(event);
            }
        } else {
            log.error("There are no event with that ID: " + eventId);
        }
    }

    @Override
    public void deletePlayerFromDbByPlayerId(Long playerId) {
        Optional<Player> playerOptionalToDelete = playerRepository.findById(playerId);
        Player player = playerOptionalToDelete.get();

        //find all events in which player participate
        Set<Long> eventIds = eventIdsFromSpecifiedPlayer(player);

        for (Long eventId : eventIds) {
            deletePlayerFromEventByEventIdAndPlayerId(eventId, playerId);
        }

        playerRepository.deleteById(playerId);

    }

    @Override
    public Set<Long> eventIdsFromSpecifiedPlayer(Player player) {
        return player.getEvents().stream().map(Event::getId).collect(Collectors.toSet());
    }


}
