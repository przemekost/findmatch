package com.przemekost.findmatch.controllers;

import com.przemekost.findmatch.commands.OrganizerCommand;
import com.przemekost.findmatch.domain.Event;
import com.przemekost.findmatch.repositories.EventRepository;
import com.przemekost.findmatch.repositories.OrganizerRepository;
import com.przemekost.findmatch.services.OrganizerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Slf4j
@Controller
public class OrganizerController {

    private OrganizerService organizerService;
    private EventRepository eventRepository;
    private OrganizerRepository organizerRepository;

    public OrganizerController(OrganizerService organizerService, EventRepository eventRepository, OrganizerRepository organizerRepository) {
        this.organizerService = organizerService;
        this.eventRepository = eventRepository;
        this.organizerRepository = organizerRepository;
    }

    @GetMapping
    @RequestMapping("/event/{eventId}/organizers/show")
    public String showListAllOrganizerForSpecifiedEvent(Model model, @PathVariable String eventId) {

        Optional<Event> eventOptional = eventRepository.findById(Long.valueOf(eventId));
        Event event = null;
        if (eventOptional.isPresent()) {
            event = eventOptional.get();
        }
        model.addAttribute("event", event);

        return "event/organizer/listOfOrganizersForSpecifiedEvent";
    }

    @RequestMapping("/organizers/show")
    public String showListOfOrganizersInDB(Model model) {

        model.addAttribute("organizersSet", organizerRepository.findAll());

        return "event/organizer/allOrganizersInDB";
    }

    @GetMapping
    @RequestMapping("/event/{eventId}/organizer/{organizerId}/show")
    public String showOrganizerByEventIdAndOrganizerId(Model model, @PathVariable String eventId, @PathVariable String organizerId) {

        model.addAttribute("organizer",
                organizerService.getOrganizerCommandByEventIdAndOrganizerId(Long.valueOf(eventId), Long.valueOf(organizerId)));
        return "event/organizer/showOrganizerForSpecifiedEvent";
    }

    @GetMapping
    @RequestMapping("/organizer/{organizerId}/show")
    public String showOrganizerByOrganizerId(Model model, @PathVariable String organizerId) {

        model.addAttribute("organizer",
                organizerService.getOrganizerById(Long.valueOf(organizerId)));
        return "event/organizer/showOrganizerForSpecifiedEvent";
    }

    @RequestMapping("/organizer/new")
    public String createNewOrganizer(Model model) {

        model.addAttribute("organizer", new OrganizerCommand());
        return "event/organizer/organizerForm";
    }

    @PostMapping
    @RequestMapping("organizer")
    public String saveOrUpdateOrganizer(@ModelAttribute OrganizerCommand organizerCommand) {

        OrganizerCommand savedOrganizer = organizerService.saveOrganizerCommand(organizerCommand);

        return "redirect:/organizer/" + savedOrganizer.getId() + "/show";
    }

    @RequestMapping("/event/{eventId}/organizer/{organizerId}/update")
    public String updateOrganizerByEventIdAndOrganizerId(Model model, @PathVariable String organizerId, @PathVariable String eventId) {

        model.addAttribute("organizer", organizerService
                .getOrganizerCommandByEventIdAndOrganizerId(Long.valueOf(eventId), Long.valueOf(organizerId)));

        return "event/organizer/organizerForm";
    }

    @RequestMapping({"event/{eventId}/organizer/{organizerId}/delete"})
    public String deleteOrganizerFromEvent(Model model, @PathVariable String organizerId, @PathVariable String eventId) {

        organizerService.deleteOrganizerFromEventByEventIdAndOrganizerId(Long.valueOf(eventId), Long.valueOf(organizerId));
        return "redirect:/event/" + eventId + "/organizers/show";
    }

    @RequestMapping({"organizer/{organizerId}/delete"})
    public String deleteOrganizerFromDB(Model model, @PathVariable String organizerId) {

        organizerService.deleteOrganizerFromDB(Long.valueOf(organizerId));

        return "redirect:/organizers/show";
    }


}