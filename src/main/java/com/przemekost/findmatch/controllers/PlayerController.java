package com.przemekost.findmatch.controllers;

import com.przemekost.findmatch.calculations.AgeCalculator;
import com.przemekost.findmatch.commands.PlayerCommand;
import com.przemekost.findmatch.domain.Event;
import com.przemekost.findmatch.repositories.EventRepository;
import com.przemekost.findmatch.services.PlayerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Slf4j
@Controller
public class PlayerController {

    private PlayerService playerService;
    private EventRepository eventRepository;
    private Event event;

    public PlayerController(PlayerService playerService, EventRepository eventRepository) {
        this.playerService = playerService;
        this.eventRepository = eventRepository;
    }

    @RequestMapping({"player/{playerId}/show"})
    public String getPlayerById(Model model, @PathVariable String playerId) {

        model.addAttribute("player", playerService.getPlayerById(Long.valueOf(playerId)));
        model.addAttribute("ageCalculator", new AgeCalculator());
        return "event/player/showPlayerById";
    }

    @RequestMapping("/event/{eventId}/player/{playerId}/show")
    public String showPlayerForSpecifiedTeam(Model model, @PathVariable String eventId, @PathVariable String playerId) {

        model.addAttribute("ageCalculator", new AgeCalculator());
        model.addAttribute("player", playerService.getPlayerByEventIdAndPlayerId(Long.valueOf(eventId), Long.valueOf(playerId)));

        return "event/player/showPlayerForSpecifiedEvent";
    }

    @GetMapping
    @RequestMapping({"/event/{eventId}/players/show"})
    public String getListOfPlayersByEventId(Model model, @PathVariable String eventId) {

        Optional<Event> eventOptional = eventRepository.findById(Long.valueOf(eventId));

        eventOptional.ifPresent(eventFromOptional -> event = eventFromOptional);

        model.addAttribute("event", event);

        return "event/player/list";
    }

    @GetMapping
    @RequestMapping({"/players"})
    public String getListOfAllPlayers(Model model) {
        model.addAttribute("players", playerService.listAllPlayers());
        return "event/player/listAllPlayersInTheDB";
    }

    @PostMapping
    @RequestMapping({"player/new"})
    public String createNewPlayer(Model model) {

        model.addAttribute("player", new PlayerCommand());

        return "event/player/playerForm";
    }

    @PostMapping
    @RequestMapping("player")
    public String saveOrUpdatePlayer(@ModelAttribute PlayerCommand playerCommand) {

        PlayerCommand savedPlayerCommand = playerService.savePlayerCommand(playerCommand);
        return "redirect:/player/" + savedPlayerCommand.getId() + "/show/";
    }

    @RequestMapping({"player/{playerId}/update"})
    public String updatePlayer(Model model, @PathVariable String playerId) {

        model.addAttribute("player", playerService.getPlayerCommandById(Long.valueOf(playerId)));

        return "event/player/playerForm";
    }

//    @RequestMapping({"event/{eventId}/player/{playerId}/update"})
//    public String updatePlayerForSpecifiedEvent(Model model, @PathVariable String playerId, @PathVariable String eventId) {
//
//        model.addAttribute("player", playerService.getPlayerByEventIdAndPlayerId(Long.valueOf(eventId), Long.valueOf(playerId)));
//
//        return "event/player/playerForm";
//    }

    @GetMapping
    @RequestMapping({"event/{eventId}/player/{playerId}/delete"})
    public String deletePlayerFromSpecificEvent(@PathVariable String eventId, @PathVariable String playerId){

        playerService.deletePlayerFromEventByEventIdAndPlayerId(Long.valueOf(eventId), Long.valueOf(playerId));

        return "redirect:/event/" + eventId + "/players/show";
    }

    @GetMapping
    @RequestMapping({"player/{playerId}/delete"})
    public String deletePlayerFromDB( @PathVariable String playerId){

        playerService.deletePlayerFromDbByPlayerId(Long.valueOf(playerId));

        return "redirect:/players";
    }


}
