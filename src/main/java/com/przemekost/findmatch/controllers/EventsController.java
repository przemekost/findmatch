package com.przemekost.findmatch.controllers;

import com.przemekost.findmatch.services.EventService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class EventsController {

    private EventService eventService;

    public EventsController(EventService eventService) {
        this.eventService = eventService;
    }

    @RequestMapping({"", "/home", "/"})
    public String getEventPage(Model model){

       model.addAttribute("events", eventService.getAllEvents());
        return "events";
    }


}
