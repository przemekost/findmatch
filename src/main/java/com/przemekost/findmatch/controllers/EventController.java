package com.przemekost.findmatch.controllers;

import com.przemekost.findmatch.commands.EventCommand;
import com.przemekost.findmatch.services.EventService;
import com.przemekost.findmatch.services.PlayerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Slf4j
@Controller
public class EventController {

    public static final String EVENT = "event";
    private EventService eventService;
    private PlayerService playerService;

    public EventController(EventService eventService, PlayerService playerService) {
        this.eventService = eventService;
        this.playerService = playerService;
    }

    @GetMapping
    @RequestMapping({"/event/{id}/show"})
    public String getEventById(Model model, @PathVariable String id) {

        model.addAttribute(EVENT, eventService.getEventById(new Long(id)));

        return "event/showEvent";
    }
    @GetMapping
    @RequestMapping("/event/new")
    public String newEvent(Model model) {

        model.addAttribute(EVENT, new EventCommand());

        return "event/eventForm";
    }
    @GetMapping
    @RequestMapping("/event/{id}/update")
    public String updateEvent(@PathVariable String id, Model model){
        model.addAttribute(EVENT, eventService.findEventCommandById(new Long(id)));

        model.addAttribute("teamList", playerService.listAllPlayers());

        return "event/eventForm";
    }

    @PostMapping
    @RequestMapping("/event")
    public String saveOrUpdate(@ModelAttribute EventCommand eventCommand){
        EventCommand savedEventCommand = eventService.saveEventCommand(eventCommand);

        return "redirect:/event/" + savedEventCommand.getId() + "/show/";
    }
    @GetMapping
    @RequestMapping("event/{id}/delete")
    public String deleteEvent(@PathVariable String id){

        log.debug("Deleting event with ID: " + id);

        eventService.deleteById(Long.valueOf(id));
        return "redirect:/";
    }


}
