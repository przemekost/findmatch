package com.przemekost.findmatch.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class FootballPitch {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String footballPitchName;

    private int minAmountOfPlayers;

    private int maxAmountOfPlayers;

    @Enumerated(value = EnumType.STRING)
    private InOrOut inOrOut;

    @Enumerated(value = EnumType.STRING)
    private Surface surface;

    @ManyToOne
    @JoinColumn(name = "LOCATION_ID")
    private Location location;
}