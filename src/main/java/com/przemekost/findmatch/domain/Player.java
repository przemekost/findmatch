package com.przemekost.findmatch.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.LocalDate;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Setter
@Getter
@EqualsAndHashCode(exclude = {"events"})
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String playerName;
    private String playerLastName;
    private String playerNickname;
    @Column(unique = true)
    private String playerEmailAddress;
    private String playerTelephoneNumber;

    //fetch type eager is required other way cause a problem:
    //Method threw 'org.hibernate.LazyInitializationException' exception. Cannot evaluate java.util.Optional.toString()
    @ManyToMany(mappedBy = "players", fetch = FetchType.EAGER)
    private Set<Event> events = new HashSet<>();

    @Enumerated(value = EnumType.STRING)
    private FavouritePosition favouritePosition;

    private String birthdayDate;
}