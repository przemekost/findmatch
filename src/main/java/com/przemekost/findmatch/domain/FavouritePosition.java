package com.przemekost.findmatch.domain;

public enum FavouritePosition {

    GOALKEEPER, DEFENDER, MIDFIELDER, ATTACKER

}
