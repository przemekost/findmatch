package com.przemekost.findmatch.domain;

public enum Surface {

    GRASS, ASPHALT, ARTIFICIAL_TURF
}
