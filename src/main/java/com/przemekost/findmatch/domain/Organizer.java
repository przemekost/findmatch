package com.przemekost.findmatch.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@EqualsAndHashCode(exclude = {"events"})
public class Organizer {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String organizerName;
    private String organizerLastName;
    @Column(unique = true)
    private String organizerEmailAddress;
    private String organizerTelephoneNumber;

    //fetch type eager is required other way cause a problem:
    //Method threw 'org.hibernate.LazyInitializationException' exception. Cannot evaluate java.util.Optional.toString()
    @ManyToMany(mappedBy = "organizers", fetch = FetchType.EAGER)
    private Set<Event> events = new HashSet<>();

}