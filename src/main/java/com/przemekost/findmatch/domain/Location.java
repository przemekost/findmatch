package com.przemekost.findmatch.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


@Entity
@Setter
@Getter
public class Location {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String cityName;

    @Column(nullable = false)
    private String streetName;

    private int streetNumber;

    private Byte[] map;

    @OneToMany(mappedBy = "location", fetch = FetchType.EAGER)
    private Set<Event> events = new HashSet<>();

    @OneToMany(mappedBy = "location", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<FootballPitch> footballPitches = new HashSet<>();


}
