package com.przemekost.findmatch.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@EqualsAndHashCode(exclude = {"players","organizers"})
@Setter
@Getter
public class Event {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String eventName;

    @Column(nullable = false)
    private LocalDateTime startTime;
    private LocalDateTime endTime;

    private Double price;

    @ManyToMany
    @JoinTable(name = "event_organizer",
            joinColumns = @JoinColumn(name = "event_id"),
            inverseJoinColumns = @JoinColumn(name = "organizer_id"))
    private Set<Organizer> organizers= new HashSet<>();


    //here cannot be cascade All
    @ManyToMany
    @JoinTable(name = "event_player",
            joinColumns = @JoinColumn(name = "event_id"),
            inverseJoinColumns = @JoinColumn(name = "player_id"))
    private Set<Player> players = new HashSet<>();

    @ManyToOne(cascade = CascadeType.ALL)
    private Location location;

    public Event addOrganizer(Organizer organizer){
        this.organizers.add(organizer);
        return this;
    }

    public Event addPlayer(Player player){
        this.players.add(player);
        return this;
    }


}