package com.przemekost.findmatch.domain;

public enum  InOrOut {

    INDOOR, OUTDOOR
}
