package com.przemekost.findmatch.repositories;

import com.przemekost.findmatch.domain.Player;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
@DataJpaTest
public class PlayerRepositoryTestIT {

    @Autowired
    PlayerRepository playerRepository;

    @Test
    public void findByPlayerName() throws Exception {

        Optional<Player> playerOptional = playerRepository.findByPlayerName("Jakub");
        assertEquals("Jakub", playerOptional.get().getPlayerName());

    }

}