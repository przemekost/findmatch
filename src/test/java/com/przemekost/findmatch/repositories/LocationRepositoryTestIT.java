//package com.przemekost.findmatch.repositories;
//
//import com.przemekost.findmatch.domain.Location;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.util.Optional;
//
//import static org.junit.Assert.*;
//
//@RunWith(SpringRunner.class)
//@DataJpaTest
//public class LocationRepositoryTestIT {
//
//    @Autowired
//    LocationRepository locationRepository;
//
//    @Test
//    public void findByCityName() throws Exception {
//
//        Optional<Location> locationOptional = locationRepository.findByCityName("Gdansk");
//        assertEquals("Gdansk", locationOptional.get().getCityName());
//
//
//    }
//
//}