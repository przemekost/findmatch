package com.przemekost.findmatch.repositories;

import com.przemekost.findmatch.domain.Organizer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class OrganizerRepositoryITTest {

    @Autowired
    OrganizerRepository organizerRepository;

    @Test
    public void findByOrOrganizerLastName() throws Exception {

        Optional<Organizer> organizerOptionalForTest = organizerRepository.findByOrOrganizerLastName("Ostrouch");
        assertEquals("Ostrouch", organizerOptionalForTest.get().getOrganizerLastName());

    }

    @Test
    public void findOrganizerById() throws Exception {

        Optional<Organizer> organizerForTest = organizerRepository.findById(1L);
        assertEquals(Long.valueOf(1L), organizerForTest.get().getId());
    }

}