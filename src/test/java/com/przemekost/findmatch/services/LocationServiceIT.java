package com.przemekost.findmatch.services;

import com.przemekost.findmatch.commands.LocationCommand;
import com.przemekost.findmatch.converters.LocationToLocationCommand;
import com.przemekost.findmatch.domain.Location;
import com.przemekost.findmatch.repositories.LocationRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LocationServiceIT {


    public final String LOCATION_CITY_NAME = "Gdansk";

    @Autowired
    LocationService locationService;

    @Autowired
    LocationRepository locationRepository;

    @Autowired
    LocationToLocationCommand locationToLocationCommand;

    @Test
    public void saveLocationCommandTest(){

        //given
        Iterable<Location> locations = locationRepository.findAll();
        Location testLocation = locations.iterator().next();
        LocationCommand testLocationCommand = locationToLocationCommand.convert(testLocation);

        //when
        testLocationCommand.setCityName(LOCATION_CITY_NAME);
        LocationCommand savedLocationCommand = locationService.saveLocationCommand(testLocationCommand);

        //then
        assertEquals(LOCATION_CITY_NAME, savedLocationCommand.getCityName());

    }

}
