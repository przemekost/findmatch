package com.przemekost.findmatch.services;

import com.przemekost.findmatch.converters.LocationCommandToLocation;
import com.przemekost.findmatch.converters.LocationToLocationCommand;
import com.przemekost.findmatch.domain.Location;
import com.przemekost.findmatch.repositories.LocationRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

public class LocationServiceImplTest {


    LocationService locationService;

    @Mock
    LocationRepository locationRepository;

    @Mock
    LocationToLocationCommand locationToLocationCommand;

    @Mock
    LocationCommandToLocation locationCommandToLocation;


    @Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);
        locationService = new LocationServiceImpl(locationRepository, locationCommandToLocation, locationToLocationCommand);

    }

    @Test
    public void getLocationById() throws Exception {

        Location location= new Location();
        location.setId(1L);

        Optional<Location> locationOptional = Optional.of(location);

        when(locationRepository.findById(anyLong())).thenReturn(locationOptional);

        Location locationReturned = locationService.getLocationById(1L);

        assertNotNull("Null locationReturned", locationReturned);
        verify(locationRepository, times(1)).findById(anyLong());
    }


}