package com.przemekost.findmatch.services;

import com.przemekost.findmatch.commands.PlayerCommand;
import com.przemekost.findmatch.converters.PlayerCommandToPlayer;
import com.przemekost.findmatch.converters.PlayerToPlayerCommand;
import com.przemekost.findmatch.domain.Event;
import com.przemekost.findmatch.domain.Player;
import com.przemekost.findmatch.repositories.EventRepository;
import com.przemekost.findmatch.repositories.PlayerRepository;
import lombok.extern.log4j.Log4j;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@Log4j
public class PlayerServiceImplTest {


    @Mock
    PlayerRepository playerRepository;

    PlayerCommandToPlayer playerCommandToPlayer = new PlayerCommandToPlayer();

    PlayerToPlayerCommand playerToPlayerCommand = new PlayerToPlayerCommand();
    @Mock
    PlayerService playerService;

    @Mock
    EventRepository eventRepository;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        playerService = new PlayerServiceImpl(playerRepository, playerCommandToPlayer, playerToPlayerCommand, eventRepository);
        }

    @Test
    public void getPlayerById() throws Exception {

        Player playerForTest = new Player();
        playerForTest.setId(1L);

        Optional<Player> playerOptional = Optional.of(playerForTest);

        when(playerRepository.findById(anyLong())).thenReturn(playerOptional);

        Player playerReturned = playerService.getPlayerById(1L);

        assertNotNull("Null playerReturned", playerReturned);
        verify(playerRepository, times(1)).findById(anyLong());

    }

    @Test
    public void getPlayerByEventIdAndPlayerIdTest() {

        Player player = new Player();
        player.setId(17L);
        player.setPlayerName("Przemek");
        Player player2 = new Player();
        player2.setId(18L);
        Set<Player> playerSet = new HashSet<>();
        playerSet.add(player);
        playerSet.add(player2);

        Event event = new Event();
        event.setPlayers(playerSet);
        Optional<Event> eventOptional = Optional.of(event);

        when(eventRepository.findById(anyLong())).thenReturn(eventOptional);

        assertEquals("Przemek", playerService.getPlayerByEventIdAndPlayerId(1L, 17L).getPlayerName());
    }

    @Test
    public void getMapOfAllPlayersInDB() {

        Set<Player> players = new HashSet<>();
        Player player = new Player();
        player.setId(1L);
        Player player1 = new Player();
        player1.setId(2L);

        players.add(player);
        players.add(player1);

        when(playerRepository.findAll()).thenReturn(players);

        Set<PlayerCommand> commands = playerService.listAllPlayers();

        assertEquals(2, commands.size());
    }

    @Test
    public void findPlayerCommandByIdTest() {

        Player player = new Player();
        player.setId(1L);
        Optional<Player> playerOptionalTest = Optional.of(player);

        when(playerRepository.findById(anyLong())).thenReturn(playerOptionalTest);

        PlayerCommand playerCommand = playerService.getPlayerCommandById(1L);

        assertNotNull("Player Command is not null", playerCommand);
    }

    @Test
    public void deletePlayerFromEventByEventIdAndPlayerIdTest(){

        Player player = new Player();
        player.setId(1L);
        Event event = new Event();
        event.addPlayer(player);
        Optional<Event> eventOptional = Optional.of(event);

        when(eventRepository.findById(anyLong())).thenReturn(eventOptional);

        playerService.deletePlayerFromEventByEventIdAndPlayerId(1L,1L);

        verify(eventRepository, times(1)).findById(anyLong());
        verify(eventRepository, times(1)).save(any(Event.class));
    }

    @Test
    public void deletePlayerFromDbByPlayerIdTest(){

        Player player = new Player();
        player.setId(1L);
        Optional<Player> playerOptional = Optional.of(player);

        when(playerRepository.findById(anyLong())).thenReturn(playerOptional);

        playerService.deletePlayerFromDbByPlayerId(1L);

        verify(playerRepository, times(1)).findById(anyLong());
        verify(playerRepository, times(1)).deleteById(anyLong());
    }

    @Test
    public void eventIdsFromSpecifiedPlayerTest(){
        Player player = new Player();
        Event event = new Event();
        event.setId(111L);
        Event event1 = new Event();
        event1.setId(444L);
        Event event2 = new Event();
        event2.setId(333L);

        Set<Event> events = new HashSet<>();
        events.add(event);
        events.add(event1);
        events.add(event2);
        player.setEvents(events);

        assertEquals(3, playerService.eventIdsFromSpecifiedPlayer(player).size());

    }
}