package com.przemekost.findmatch.services;

import com.przemekost.findmatch.converters.EventCommandToEvent;
import com.przemekost.findmatch.converters.EventToEventCommand;
import com.przemekost.findmatch.domain.Event;
import com.przemekost.findmatch.repositories.EventRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

public class EventServiceImplTest {

    EventServiceImpl eventService;

    @Mock
    EventRepository eventRepository;

    @Mock
    EventCommandToEvent eventCommandToEvent;

    @Mock
    EventToEventCommand eventToEventCommand;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        eventService = new EventServiceImpl(eventRepository, eventCommandToEvent, eventToEventCommand);
    }

    @Test
    public void getEvents() throws Exception {

        Set<Event> eventsForTest = new HashSet<>();
        eventsForTest.add(new Event());

        when(eventService.getAllEvents()).thenReturn(eventsForTest);

        Set<Event> events = eventService.getAllEvents();

        assertEquals(1, events.size());
        verify(eventRepository, times(1)).findAll();

    }

    @Test
    public void findEventByIdTest() throws Exception {

        Event eventForTest = new Event();
        eventForTest.setId(1L);
        Optional<Event> eventOptional = Optional.of(eventForTest);

        when(eventRepository.findById(anyLong())).thenReturn(eventOptional);

        Event eventReturned = eventService.getEventById(1L);

        assertNotNull("Null event returned", eventReturned);
        verify(eventRepository, times(1)).findById(anyLong());

    }

    @Test
    public void deleteEventByIdTest(){

        Long idToDelete = Long.valueOf(2L);

        eventService.deleteById(idToDelete);

        verify(eventRepository, times(1)).deleteById(anyLong());

    }

}
