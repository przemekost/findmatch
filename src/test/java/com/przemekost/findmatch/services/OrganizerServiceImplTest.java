package com.przemekost.findmatch.services;

import com.przemekost.findmatch.commands.OrganizerCommand;
import com.przemekost.findmatch.converters.OrganizerCommandToOrganizer;
import com.przemekost.findmatch.converters.OrganizerToOrganizerCommand;
import com.przemekost.findmatch.domain.Event;
import com.przemekost.findmatch.domain.Organizer;
import com.przemekost.findmatch.repositories.EventRepository;
import com.przemekost.findmatch.repositories.OrganizerRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@Slf4j
public class OrganizerServiceImplTest {

    OrganizerServiceImpl organizerService;

    @Mock
    OrganizerRepository organizerRepository;

    OrganizerCommandToOrganizer organizerCommandToOrganizer;

    OrganizerToOrganizerCommand organizerToOrganizerCommand;

    @Mock
    EventRepository eventRepository;

    public OrganizerServiceImplTest() {
        this.organizerToOrganizerCommand = new OrganizerToOrganizerCommand();
    }

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        organizerService = new OrganizerServiceImpl(organizerRepository,
                organizerCommandToOrganizer,
                organizerToOrganizerCommand,
                eventRepository);
    }


    @Test
    public void findOrganizerByIdTest() throws Exception {

        Organizer organizerForTest = new Organizer();
        organizerForTest.setId(1L);


        Optional<Organizer> organizerOptional = Optional.of(organizerForTest);


        when(organizerRepository.findById(anyLong())).thenReturn(organizerOptional);

        Organizer organizerReturned = organizerService.getOrganizerById(1L);

        assertNotNull("Null organizerReturned returned", organizerReturned);
        verify(organizerRepository, times(1)).findById(anyLong());
    }

    @Test
    public void findOrganizerCommandByEventIdAndOrganizerIdTest() {

        Event event = new Event();
        event.setId(1L);

        Organizer organizer1 = new Organizer();
        organizer1.setId(2L);

        Organizer organizer2 = new Organizer();
        organizer2.setId(4L);

        Organizer organizer3 = new Organizer();
        organizer3.setId(6L);

        event.addOrganizer(organizer1);
        event.addOrganizer(organizer2);
        event.addOrganizer(organizer3);

        Optional<Event> optionalEvent = Optional.of(event);
        when(eventRepository.findById(anyLong())).thenReturn(optionalEvent);


        OrganizerCommand organizerCommand = organizerService.getOrganizerCommandByEventIdAndOrganizerId(1L, 4L);

        assertEquals(Long.valueOf(4L), organizerCommand.getId());
        verify(eventRepository, times(1)).findById(anyLong());
    }

}