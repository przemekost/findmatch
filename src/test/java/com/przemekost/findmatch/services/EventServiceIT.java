package com.przemekost.findmatch.services;

import com.przemekost.findmatch.commands.EventCommand;
import com.przemekost.findmatch.converters.EventCommandToEvent;
import com.przemekost.findmatch.converters.EventToEventCommand;
import com.przemekost.findmatch.domain.Event;
import com.przemekost.findmatch.repositories.EventRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
@SpringBootTest
public class EventServiceIT {

    public static final String EVENT_NAME = "Kołobrzeska Event";

    @Autowired
    EventService eventService;

    @Autowired
    EventRepository eventRepository;

    @Autowired
    EventCommandToEvent eventCommandToEvent;

    @Autowired
    EventToEventCommand eventToEventCommand;


    @Transactional
    @Test
    public void saveOfEventName(){
        Iterable<Event> events = eventRepository.findAll();
        Event testEvent = events.iterator().next();
        EventCommand testEventCommand = eventToEventCommand.convert(testEvent);

        testEventCommand.setEventName(EVENT_NAME);
        EventCommand savedEventCommand = eventService.saveEventCommand(testEventCommand);

        assertEquals(EVENT_NAME, savedEventCommand.getEventName());
        assertEquals(testEvent.getId(), savedEventCommand.getId());
    }

}
