package com.przemekost.findmatch.services;

import com.przemekost.findmatch.commands.OrganizerCommand;
import com.przemekost.findmatch.converters.OrganizerToOrganizerCommand;
import com.przemekost.findmatch.domain.Organizer;
import com.przemekost.findmatch.repositories.OrganizerRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrganizerServiceIT {

    public static final String ORGANIZER_NAME = "Hnatyszyn";

    @Autowired
    OrganizerRepository organizerRepository;

    @Autowired
    OrganizerToOrganizerCommand organizerToOrganizerCommand;

    @Autowired
    OrganizerService organizerService;

    @Test
    public void saveOrganizerCommandTest(){
        //given
        Iterable<Organizer> organizers = organizerRepository.findAll();
        Organizer organizerTest = organizers.iterator().next();
        OrganizerCommand organizerCommand = organizerToOrganizerCommand.convert(organizerTest);

        //when
        organizerCommand.setOrganizerName(ORGANIZER_NAME);
        OrganizerCommand savedOrganizerCommand = organizerService.saveOrganizerCommand(organizerCommand);

        //then
        assertEquals(ORGANIZER_NAME, savedOrganizerCommand.getOrganizerName());


    }

}
