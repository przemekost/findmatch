package com.przemekost.findmatch.services;

import com.przemekost.findmatch.commands.PlayerCommand;
import com.przemekost.findmatch.converters.PlayerToPlayerCommand;
import com.przemekost.findmatch.domain.Player;
import com.przemekost.findmatch.repositories.PlayerRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PlayerServiceIT {

    private final static String PLAYER_NAME = "Przemyslaw";

    @Autowired
    PlayerRepository playerRepository;
    @Autowired
    PlayerService playerService;
    @Autowired
    PlayerToPlayerCommand playerToPlayerCommand;



    @Test
    public void savePlayerCommandTest(){
        Iterable<Player> players = playerRepository.findAll();
        Player player = players.iterator().next();
        PlayerCommand playerCommand = playerToPlayerCommand.convert(player);

        playerCommand.setPlayerLastName(PLAYER_NAME);
        PlayerCommand savedPlayerCommand = playerService.savePlayerCommand(playerCommand);

        assertEquals(PLAYER_NAME, savedPlayerCommand.getPlayerName());

    }


}
