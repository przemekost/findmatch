package com.przemekost.findmatch.converters;

import com.przemekost.findmatch.commands.PlayerCommand;
import com.przemekost.findmatch.domain.Event;
import com.przemekost.findmatch.domain.FavouritePosition;
import com.przemekost.findmatch.domain.Player;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Slf4j
public class PlayerToPlayerCommandTest {

    private static final String PLAYER_NAME = "Michal";
    private static final String PLAYER_LAST_NAME = "Rzepczyk";
    private static final Long PLAYER_ID = 2L;
    private static final int PLAYER_AGE = 22;
    private static final String PLAYER_PHONE_NUMBER = "444-444-444";
    private static final FavouritePosition FAVOURITE_POSITION = FavouritePosition.ATTACKER;

    PlayerToPlayerCommand playerToPlayerCommand;

    @Before
    public void setup() {
        playerToPlayerCommand = new PlayerToPlayerCommand();
    }

    @Test
    public void convertTest() throws Exception {

        Event event = new Event();
        event.setId(44L);
        Event event1 = new Event();
        event1.setId(45L);
        Set<Event> events = new HashSet<>(2);
        events.add(event);
        events.add(event1);

        Player player = new Player();
        player.setPlayerName(PLAYER_NAME);
        player.setId(PLAYER_ID);
        player.setPlayerLastName(PLAYER_LAST_NAME);
        player.setFavouritePosition(FAVOURITE_POSITION);
        player.setPlayerTelephoneNumber(PLAYER_PHONE_NUMBER);
        player.setEvents(events);

        PlayerCommand playerCommand = playerToPlayerCommand.convert(player);

        assertNotNull(playerCommand);
        assertEquals(PLAYER_ID, playerCommand.getId());
        assertEquals(PLAYER_LAST_NAME, playerCommand.getPlayerLastName());
        assertEquals(PLAYER_PHONE_NUMBER, playerCommand.getPlayerTelephoneNumber());
        assertEquals(FAVOURITE_POSITION, playerCommand.getFavouritePosition());
        assertEquals(2, playerCommand.getEvents().size());
    }

}