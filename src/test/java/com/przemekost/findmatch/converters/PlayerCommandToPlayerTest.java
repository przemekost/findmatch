package com.przemekost.findmatch.converters;

import com.przemekost.findmatch.commands.PlayerCommand;
import com.przemekost.findmatch.domain.FavouritePosition;
import com.przemekost.findmatch.domain.Player;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PlayerCommandToPlayerTest {

    private static final String PLAYER_NAME = "Michal";
    private static final String PLAYER_LAST_NAME = "Rzepczyk";
    private static final Long PLAYER_ID = 2L;
    private static final int PLAYER_AGE = 22;
    private static final String PLAYER_PHONE_NUMBER = "444-444-444";
    private static final FavouritePosition FAVOURITE_POSITION = FavouritePosition.ATTACKER;

    PlayerCommandToPlayer converter;

    @Before
    public void setUp() throws Exception {

        converter = new PlayerCommandToPlayer();

    }

    @Test
    public void convert() throws Exception {

        PlayerCommand command = new PlayerCommand();
        command.setId(PLAYER_ID);
        command.setPlayerName(PLAYER_NAME);
        command.setFavouritePosition(FAVOURITE_POSITION);
        command.setPlayerLastName(PLAYER_LAST_NAME);
        command.setPlayerTelephoneNumber(PLAYER_PHONE_NUMBER);

        Player playerFromCommand = converter.convert(command);

        assertEquals(PLAYER_ID, playerFromCommand.getId());
        assertEquals(PLAYER_NAME, playerFromCommand.getPlayerName());
        assertEquals(PLAYER_LAST_NAME, playerFromCommand.getPlayerLastName());
        assertEquals(PLAYER_PHONE_NUMBER, playerFromCommand.getPlayerTelephoneNumber());
        assertEquals(FAVOURITE_POSITION, playerFromCommand.getFavouritePosition());
    }

    @Test
    public void nullParamTest(){
        assertNull(converter.convert(null));
    }

    @Test
    public void emptyObjectTest(){
        assertNotNull(converter.convert(new PlayerCommand()));
    }
}