package com.przemekost.findmatch.converters;

import com.przemekost.findmatch.commands.OrganizerCommand;
import com.przemekost.findmatch.domain.Organizer;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class OrganizerCommandToOrganizerTest {

    OrganizerCommandToOrganizer converter;

    @Before
    public void setUp() throws Exception {

        converter = new OrganizerCommandToOrganizer();
    }

    @Test
    public void convert() throws Exception {

        OrganizerCommand command = new OrganizerCommand();
        command.setId(Long.valueOf(1L));
        command.setOrganizerEmailAddress("1@przem.com");

        Organizer organizerAfterConversion = converter.convert(command);

        assertEquals(Long.valueOf(1L),organizerAfterConversion.getId());
        assertNotNull(organizerAfterConversion.getOrganizerEmailAddress());

    }

    @Test
    public void nullParamTest(){
        assertNull(converter.convert(null));
    }

    @Test
    public void emptyObjectTest(){
        assertNotNull(converter.convert(new OrganizerCommand()));
    }
}