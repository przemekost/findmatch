package com.przemekost.findmatch.converters;

import com.przemekost.findmatch.commands.FootballPitchCommand;
import com.przemekost.findmatch.commands.LocationCommand;
import com.przemekost.findmatch.domain.Location;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LocationCommandToLocationTest {

    LocationCommandToLocation converter;

    @Before
    public void setUp() throws Exception {

        converter = new LocationCommandToLocation(new FootballPitchCommandToFootballPitch());
    }

    @Test
    public void convert() throws Exception {

        LocationCommand command = new LocationCommand();
        command.setId(1L);
        command.setCityName("Ujejsce");
        command.setStreetName("Ustronie");
        FootballPitchCommand footballPitch1 = new FootballPitchCommand();
        footballPitch1.setId(1L);
        FootballPitchCommand footballPitch2 = new FootballPitchCommand();
        footballPitch2.setId(2L);

        command.getFootballPitches().add(footballPitch1);
        command.getFootballPitches().add(footballPitch2);


        Location locationFromConvert = converter.convert(command);

        assertEquals(1L ,locationFromConvert.getId());
        assertNotNull(locationFromConvert);
        assertEquals("Ujejsce", locationFromConvert.getCityName());
        assertEquals("Ustronie", locationFromConvert.getStreetName());
        assertEquals(2, locationFromConvert.getFootballPitches().size());


    }

    @Test
    public void emptyObjectTest(){
        assertNotNull(converter.convert(new LocationCommand()));
    }

    @Test
    public void nullParamTest(){

        assertNull(converter.convert(null));
    }


}