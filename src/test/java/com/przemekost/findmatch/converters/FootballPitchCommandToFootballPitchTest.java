package com.przemekost.findmatch.converters;

import com.przemekost.findmatch.commands.FootballPitchCommand;
import com.przemekost.findmatch.domain.FootballPitch;
import com.przemekost.findmatch.domain.InOrOut;
import com.przemekost.findmatch.domain.Surface;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(initializers = ConfigFileApplicationContextInitializer.class)
@ActiveProfiles("test")
public class FootballPitchCommandToFootballPitchTest {

    FootballPitchCommandToFootballPitch converter;

    @Value("${FOOTBALL_PITCH_NAME}")
    private String footballPitchName ;

    @Before
    public void setUp() throws Exception {

        converter = new FootballPitchCommandToFootballPitch();

        System.out.println(footballPitchName);
    }
    @Test
    public void nullParameterTest()throws Exception{
        assertNull(converter.convert(null));
    }

    @Test
    public void convert() throws Exception {

        FootballPitchCommand command = new FootballPitchCommand();
        command.setId(1L);
        command.setFootballPitchName(footballPitchName);
        command.setSurface(Surface.ARTIFICIAL_TURF);
        command.setInOrOut(InOrOut.OUTDOOR);

        FootballPitch footballPitch = converter.convert(command);

        assertNotNull(footballPitch);
        assertEquals(Surface.ARTIFICIAL_TURF, footballPitch.getSurface());
        assertEquals(footballPitchName, footballPitch.getFootballPitchName());
        assertEquals(InOrOut.OUTDOOR, footballPitch.getInOrOut());

    }

    @Test
    public void objectEmptyTest() {
        assertNotNull(converter.convert(new FootballPitchCommand()));
    }



}