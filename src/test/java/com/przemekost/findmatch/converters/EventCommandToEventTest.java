package com.przemekost.findmatch.converters;

import com.przemekost.findmatch.commands.EventCommand;
import com.przemekost.findmatch.commands.LocationCommand;
import com.przemekost.findmatch.commands.OrganizerCommand;
import com.przemekost.findmatch.domain.Event;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.Month;

import static org.junit.Assert.*;


public class EventCommandToEventTest {


    EventCommandToEvent eventCommandToEvent;


    public final static LocalDateTime START_TIME = LocalDateTime.of(2017, Month.NOVEMBER, 15, 19,00,00);
    public final static LocalDateTime END_TIME = LocalDateTime.of(2017, Month.NOVEMBER, 15, 20,00,00);

    @Before
    public void setUp() throws Exception {


    }

    @Test
    public void convert() throws Exception {

        EventCommand eventCommand = new EventCommand();
        eventCommand.setId(1L);
        eventCommand.setEventName("Match");
        eventCommand.setStartTime(START_TIME);
        eventCommand.setEndTime(END_TIME);
        eventCommand.setPrice(12.20);

        OrganizerCommand organizerCommand = new OrganizerCommand();
        organizerCommand.setId(1L);

        OrganizerCommand organizer2Command = new OrganizerCommand();
        organizer2Command.setId(2L);

        eventCommand.getOrganizers().add(organizerCommand);
        eventCommand.getOrganizers().add(organizer2Command);


        LocationCommand locationCommand = new LocationCommand();
        locationCommand.setId(1L);

        eventCommand.setLocation(locationCommand);


        Event eventFromCommand = eventCommandToEvent.convert(eventCommand);

        assertNotNull(eventFromCommand);
        assertEquals(1L, eventFromCommand.getId());
        assertEquals(2, eventFromCommand.getOrganizers().size());
        assertEquals("Match", eventFromCommand.getEventName());
        assertEquals(START_TIME, eventFromCommand.getStartTime());
        assertEquals(END_TIME, eventFromCommand.getEndTime());
        Double expectedPrice = 12.2;
        assertEquals(expectedPrice, eventFromCommand.getPrice());
        assertEquals(1L, eventFromCommand.getLocation().getId());
    }

    @Test
    public void emptyObjectTest(){
        assertNotNull(eventCommandToEvent.convert(new EventCommand()));
    }

    @Test
    public void nullParameterTest(){
        assertNull(eventCommandToEvent.convert(null));
    }

}
