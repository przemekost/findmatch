package com.przemekost.findmatch.calculations;

import com.przemekost.findmatch.commands.PlayerCommand;
import com.przemekost.findmatch.domain.Player;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
@Slf4j
public class AgeCalculatorTest {


    private Player playerForTest;

    private AgeCalculator ageCalculator = new AgeCalculator();
    private String date;


    @Before
    public void setUp(){
        date = "09/24/1988";
    }
    @Test
    public void calculateAge() throws Exception {
        int expectedAge = 29;
        assertEquals(expectedAge, ageCalculator.calculateAge(date));


    }

}