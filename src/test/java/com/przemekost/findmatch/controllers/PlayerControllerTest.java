package com.przemekost.findmatch.controllers;

import com.przemekost.findmatch.calculations.AgeCalculator;
import com.przemekost.findmatch.commands.PlayerCommand;
import com.przemekost.findmatch.repositories.EventRepository;
import com.przemekost.findmatch.services.PlayerService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class PlayerControllerTest {

    private MockMvc mockMvc;
    private PlayerController playerController;
    @Mock
    private PlayerService playerService;
    @Mock
    private EventRepository eventRepository;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        playerController = new PlayerController(playerService, eventRepository);
        mockMvc = MockMvcBuilders.standaloneSetup(playerController).build();
    }
    @Test
    public void getListOfPlayersByEventIdTest() throws Exception {

        mockMvc.perform(get("/event/1/players/show"))
                .andExpect(status().isOk())
                .andExpect(view().name("event/player/list"));
    }

    @Test
    public void getListOfAllPlayersTest() throws Exception {

        mockMvc.perform(get("/players"))
                .andExpect(status().isOk())
                .andExpect(view().name("event/player/listAllPlayersInTheDB"));
    }

    @Test
    public void createNewPlayerControllerTest() throws Exception {

        mockMvc.perform(get("/player/new"))
                .andExpect(status().isOk())
                .andExpect(view().name("event/player/playerForm"));
    }

    @Test
    public void updatePlayerControllerTest() throws Exception {

        PlayerCommand playerCommand = new PlayerCommand();
        playerCommand.setId(1L);

        when(playerService.getPlayerCommandById(anyLong())).thenReturn(playerCommand);

        mockMvc.perform(get("/player/1/update"))
                .andExpect(status().isOk())
                .andExpect(view().name("event/player/playerForm"))
                .andExpect(model().attributeExists("player"));

        verify(playerService, times(1)).getPlayerCommandById(anyLong());
    }

    @Test
    public void showPlayerForSpecifiedTeamTest() throws Exception {

        mockMvc.perform(get("/event/1/player/2/show"))
                .andExpect(status().isOk())
                .andExpect(view().name("event/player/showPlayerForSpecifiedEvent"));
    }

    @Test
    public void updatePlayerForSpecifiedEventTest() throws Exception {

        mockMvc.perform(get("/event/1/player/2/update"))
                .andExpect(status().isOk())
                .andExpect(view().name("event/player/playerForm"));

        verify(playerService, times(1)).getPlayerByEventIdAndPlayerId(anyLong(), anyLong());

    }
}
