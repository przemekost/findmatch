package com.przemekost.findmatch.controllers;

import com.przemekost.findmatch.commands.EventCommand;
import com.przemekost.findmatch.domain.Event;
import com.przemekost.findmatch.services.EventServiceImpl;
import com.przemekost.findmatch.services.PlayerService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class EventControllerTest {

    EventController eventController;

    MockMvc mockMvc;

    @Mock
    EventServiceImpl eventService;

    @Mock
    PlayerService playerService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        eventController = new EventController(eventService, playerService);
        mockMvc = MockMvcBuilders.standaloneSetup(eventController).build();
    }

    @Test
    public void showEventByIdTest() throws Exception {
        Event eventForTest = new Event();
        eventForTest.setId(1L);

        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(eventController).build();

        when(eventService.getEventById(anyLong())).thenReturn(eventForTest);

        mockMvc.perform(get("/event/1/show"))
                .andExpect((status().isOk()))
                .andExpect(view().name("event/showEvent"));
    }

    @Test
    public void getNewEventFormTest() throws Exception {
        EventCommand eventCommand = new EventCommand();

        mockMvc.perform(get("/event/new"))
                .andExpect(status().isOk())
                .andExpect(view().name("event/eventForm"))
                .andExpect(model().attributeExists("event"));
    }
        //TODO
//    @Test
//    public void postNewEventFormTest() throws Exception {
//        EventCommand eventCommand = new EventCommand();
//        eventCommand.setId(2L);
//
//        when(eventService.saveEventCommand(any())).thenReturn(eventCommand);
//
//        mockMvc.perform(post("/event")
//                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
//                .param("id", "")
//                .param("description", "some string")
//        )
//                .andExpect(status().is3xxRedirection())
//                .andExpect(view().name("redirect:/event/2/show"));
//    }

    @Test
    public void getUpdateViewTest() throws Exception {
        EventCommand eventCommand = new EventCommand();
        eventCommand.setId(2L);

        when(eventService.findEventCommandById(anyLong())).thenReturn(eventCommand);

        mockMvc.perform(get("/event/1/update"))
                .andExpect(status().isOk())
                .andExpect(view().name("event/eventForm"))
                .andExpect(model().attributeExists("event"));

    }

    @Test
    public void deleteEventByIdControllerTest() throws Exception {

        mockMvc.perform(get("/event/1/delete"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/"));

        verify(eventService, times(1)).deleteById(anyLong());

    }

}
