package com.przemekost.findmatch.controllers;

import com.przemekost.findmatch.domain.Event;
import com.przemekost.findmatch.services.EventService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.Model;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class EventsControllerTest {


    EventsController eventsController;

    @Mock
    EventService eventService;

    @Mock
    Model model;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        eventsController = new EventsController(eventService);
    }

    @Test
    public void testMockMVC() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(eventsController).build();

        mockMvc.perform(get(""))
                .andExpect((status().isOk()))
                .andExpect(view().name("events"));

        verify(eventService, times(1)).getAllEvents();
    }

    @Test
    public void getEventPage() throws Exception {

        Set<Event> events = new HashSet<>();
        events.add(new Event());
        Event event2 = new Event();
        event2.setPrice(2.0);
        events.add(event2);

        when(eventService.getAllEvents()).thenReturn(events);

        ArgumentCaptor<Set<Event>> argumentCaptor = ArgumentCaptor.forClass(Set.class);



        String expectedViewName = "events";

        assertEquals(expectedViewName, eventsController.getEventPage(model));
        verify(eventService, times(1)).getAllEvents();
        verify(model, times(1)).addAttribute(eq("events"), argumentCaptor.capture());


        Set<Event> setInController = argumentCaptor.getValue();
        assertEquals(2, setInController.size());
    }

}