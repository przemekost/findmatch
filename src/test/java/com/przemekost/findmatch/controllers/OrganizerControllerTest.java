package com.przemekost.findmatch.controllers;

import com.przemekost.findmatch.repositories.EventRepository;
import com.przemekost.findmatch.repositories.OrganizerRepository;
import com.przemekost.findmatch.services.OrganizerService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class OrganizerControllerTest {

    OrganizerController organizerController;

    private MockMvc mockMvc;

    @Mock
    private OrganizerService organizerService;

    @Mock
    private EventRepository eventRepository;

    @Mock
    private OrganizerRepository organizerRepository;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        organizerController = new OrganizerController(organizerService, eventRepository, organizerRepository);
        mockMvc = MockMvcBuilders.standaloneSetup(organizerController).build();

    }

    @Test
    public void showOrganizerByEventIdAndOrganizerIdTest() throws Exception {

        mockMvc.perform(get("/event/1/organizer/2/show"))
                .andExpect(status().isOk())
                .andExpect(view().name("event/organizer/showOrganizerForSpecifiedEvent"));
    }


    @Test
    public void showListAllOrganizerForSpecifiedEventTest() throws Exception {

        mockMvc.perform(get("/event/1/organizers/show"))
                .andExpect(status().isOk())
                .andExpect(view().name("event/organizer/listOfOrganizersForSpecifiedEvent"));
    }
}